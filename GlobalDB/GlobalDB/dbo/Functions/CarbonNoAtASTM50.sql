﻿CREATE FUNCTION dbo.CarbonNoAtASTM50(@ASTM50 real) 
RETURNS real
AS
BEGIN
	DECLARE @CNo real

	DECLARE @BP1 real, @CNo1 real, @BP2 real, @CNo2 real
	SELECT @BP1 = BoilingPt_F, @CNo1 = CarbonNo
	FROM nParaffinBoilPt a
	WHERE BoilingPt_F <= @ASTM50 AND NOT EXISTS (SELECT * FROM nParaffinBoilPt b WHERE b.BoilingPt_F > a.BoilingPt_F AND b.BoilingPt_F <= @ASTM50)

	SELECT @BP2 = BoilingPt_F, @CNo2 = CarbonNo
	FROM nParaffinBoilPt a
	WHERE BoilingPt_F >= @ASTM50 AND NOT EXISTS (SELECT * FROM nParaffinBoilPt b WHERE b.BoilingPt_F < a.BoilingPt_F AND b.BoilingPt_F >= @ASTM50)

	IF @CNo1 IS NULL
		SET @CNo = @CNo2
	ELSE IF @CNo2 IS NULL
		SET @CNo = @CNo1
	ELSE IF @CNo1 = @CNo2
		SET @CNo = @CNo1
	ELSE
		SELECT @CNo = @CNo1 + (@CNo2-@CNo1)*(@ASTM50 - @BP1)/(@BP2 - @BP1)

	RETURN @CNo
END

