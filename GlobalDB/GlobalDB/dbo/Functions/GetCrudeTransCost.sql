﻿
CREATE Function [dbo].[GetCrudeTransCost](@StudyYear StudyYear, @CNum varchar(5), @ActualAPI real, @CrudeDest PricingLocation, @Period Period = 'TY')
RETURNS real
AS
BEGIN
	DECLARE @BasePrice real, @TransCost real, @CrudeOrigin PricingLocation, @BblAdder real, @MTAdder real, @PcntAdj real

	SELECT 	@BasePrice = dbo.GetCrudeBasePrice(@StudyYear, @CNum, @Period)
		, @CrudeOrigin = dbo.GetCrudeOrigin(@StudyYear, @CNum, @Period)

	DECLARE @MTPerBbl float
	SELECT @MTPerBbl = dbo.UnitsConv(dbo.UnitsConv(@ActualAPI, 'API', 'KGM3'), 'KG/M3', 'MT/B')

	SELECT	@BblAdder = SUM(CASE WHEN TL.AdjMethod='+' THEN T.AdjValue ELSE 0 END), 
			@MTAdder = SUM(CASE WHEN TL.AdjMethod='+' THEN T.AdjValueMT ELSE 0 END), 
			@PcntAdj = SUM(CASE WHEN TL.AdjMethod='%' THEN T.AdjValue ELSE 0 END)
	FROM Transportation T INNER JOIN TransAdjType_LU TL ON T.AdjType = TL.AdjType
	WHERE T.StudyYear = @StudyYear AND T.Period = @Period AND T.CrudeOrigin = @CrudeOrigin AND T.CrudeDest = @CrudeDest 

	SELECT @TransCost = ISNULL(@BblAdder, 0) + ISNULL(@MTAdder*@MTPerBbl, 0) + ISNULL(@PcntAdj*@BasePrice/100, 0)
	RETURN @TransCost
END

