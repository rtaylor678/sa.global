﻿CREATE FUNCTION [dbo].[WalshMortimerAnilinePt](@ASTM50 real, @SpecGravity real)
RETURNS real
AS
BEGIN
	DECLARE @AnilinePt real, @CNo real
	SELECT @CNo = dbo.CarbonNoAtASTM50(@ASTM50)
	SELECT @AnilinePt = -204.9 - (1.498*@CNo) + 100.5*POWER(@CNo, 1.0/3)/@SpecGravity
	SELECT @AnilinePt = dbo.UnitsConv(@AnilinePt, 'TC', 'TF')
	RETURN @AnilinePt
END


