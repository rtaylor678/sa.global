﻿CREATE Function dbo.GetCrudeBasePrice(@StudyYear StudyYear, @CNum varchar(5), @Period Period = 'TY')
RETURNS real
AS
BEGIN
	DECLARE @BasePrice real
	SELECT 	@BasePrice = P.BasePrice
	FROM CrudePrice P
	WHERE P.StudyYear = @StudyYear AND P.Period = @Period AND P.CNum = @CNum
	
	RETURN @BasePrice
END
