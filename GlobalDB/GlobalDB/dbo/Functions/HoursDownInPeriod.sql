﻿
CREATE FUNCTION [dbo].[HoursDownInPeriod](@StartDown smalldatetime, @EndDown smalldatetime, @StartPeriod smalldatetime, @EndPeriod smalldatetime)
RETURNS real
BEGIN
	DECLARE @LaterStart smalldatetime, @FirstEnd smalldatetime, @hrs real
	SELECT @hrs = 0, @LaterStart = CASE WHEN @StartDown < @StartPeriod THEN @StartPeriod ELSE @StartDown END,
		@FirstEnd = CASE WHEN @EndDown > @EndPeriod THEN @EndPeriod ELSE @EndDown END
	SELECT @hrs = DATEDIFF(hh, @LaterStart, @FirstEnd)
	IF @hrs < 0
		SELECT @hrs = 0
	RETURN @hrs
END

