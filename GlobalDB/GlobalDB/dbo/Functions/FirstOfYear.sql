﻿
CREATE FUNCTION dbo.FirstOfYear(@Year smallint)
RETURNS smalldatetime
BEGIN
	DECLARE @dt smalldatetime
	SET @dt = CAST('1/1/' + CAST(@Year as varchar(4)) as smalldatetime)
	RETURN @dt
END
