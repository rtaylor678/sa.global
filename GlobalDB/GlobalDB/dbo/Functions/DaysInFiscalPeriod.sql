﻿
CREATE function [dbo].[DaysInFiscalPeriod](@FiscalYearStartDate smalldatetime, @Period Period)
RETURNS int
BEGIN
	DECLARE @d int
	DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime
	IF @Period = 'TY'
		SELECT @PeriodStart = @FiscalYearStartDate, @PeriodEnd = DATEADD(YY, 1, @FiscalYearStartDate)
	ELSE IF @Period = 'Q1'
		SELECT @PeriodStart = @FiscalYearStartDate, @PeriodEnd = DATEADD(M, 3, @FiscalYearStartDate)
	ELSE IF @Period = 'Q2'
		SELECT @PeriodStart = DATEADD(M, 3, @FiscalYearStartDate), @PeriodEnd = DATEADD(M, 6, @FiscalYearStartDate)
	ELSE IF @Period = 'Q3'
		SELECT @PeriodStart = DATEADD(M, 6, @FiscalYearStartDate), @PeriodEnd = DATEADD(M, 9, @FiscalYearStartDate)
	ELSE IF @Period = 'Q4'
		SELECT @PeriodStart = DATEADD(M, 9, @FiscalYearStartDate), @PeriodEnd = DATEADD(M, 12, @FiscalYearStartDate)
	ELSE 
		SELECT @PeriodStart = NULL, @PeriodEnd = NULL
	SELECT @d = DATEDIFF(d, @PeriodStart, @PeriodEnd)
	RETURN @d
END

