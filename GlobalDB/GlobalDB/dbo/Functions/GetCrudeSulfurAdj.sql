﻿
CREATE Function dbo.GetCrudeSulfurAdj(@StudyYear StudyYear, @CNum varchar(5), @ActualSulfur real, @Period Period = 'TY')
RETURNS real
AS
BEGIN
	DECLARE @SulfurAdj real, @PostedSulfur real, @SulfurAdjSlope real

	SELECT 	@PostedSulfur = P.PostedSulfur, @SulfurAdjSlope = P.SulfurAdjSlope
	FROM CrudePrice P
	WHERE P.StudyYear = @StudyYear AND P.Period = @Period AND P.CNum = @CNum

	SELECT @SulfurAdj = ISNULL((@PostedSulfur-@ActualSulfur)*@SulfurAdjSlope,0)
	
	RETURN @SulfurAdj
END
