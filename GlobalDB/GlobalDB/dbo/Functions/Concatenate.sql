﻿CREATE AGGREGATE [dbo].[Concatenate](@valueA NVARCHAR (200))
    RETURNS NVARCHAR (MAX)
    EXTERNAL NAME [CustomAggregates].[CustomAggregates.Concatenate];

