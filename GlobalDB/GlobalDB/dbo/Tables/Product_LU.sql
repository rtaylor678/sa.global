﻿CREATE TABLE [dbo].[Product_LU] (
    [ProductID]      CHAR (5)     NOT NULL,
    [Name]           VARCHAR (50) NULL,
    [TypicalDensity] REAL         NULL,
    CONSTRAINT [PK___2__15] PRIMARY KEY CLUSTERED ([ProductID] ASC) WITH (FILLFACTOR = 90)
);

