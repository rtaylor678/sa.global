﻿CREATE TABLE [dbo].[BlendingValues] (
    [StudyYear] [dbo].[StudyYear] NOT NULL,
    [PriceLoc]  SMALLINT          NOT NULL,
    [Product]   CHAR (10)         NOT NULL,
    [MultBBL]   REAL              NULL,
    [MultMT]    REAL              NULL,
    CONSTRAINT [PK_BlendingValues] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [PriceLoc] ASC, [Product] ASC)
);

