﻿CREATE TABLE [dbo].[Employees] (
    [Initials]     CHAR (4)  NOT NULL,
    [EmployeeName] CHAR (30) NOT NULL,
    CONSTRAINT [PK_Employees_1__16] PRIMARY KEY CLUSTERED ([Initials] ASC) WITH (FILLFACTOR = 90)
);

