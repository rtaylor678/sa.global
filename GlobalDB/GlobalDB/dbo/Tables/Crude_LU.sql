﻿CREATE TABLE [dbo].[Crude_LU] (
    [CNum]          [dbo].[CrudeNum] NOT NULL,
    [CrudeName]     VARCHAR (50)     NULL,
    [TypicalAPI]    REAL             NULL,
    [TypicalSulfur] REAL             NULL,
    [ProdCountry]   VARCHAR (25)     NULL,
    [ProdState]     VARCHAR (25)     NULL,
    [SortKey]       SMALLINT         NULL,
    [Display]       [dbo].[YorN]     NULL,
    [UOPNum]        [dbo].[CrudeNum] NULL,
    [GapCategory]   VARCHAR (4)      NULL,
    [TANNum]        REAL             NULL,
    [BPTANNum]      REAL             NULL,
    [GenericCNum]   VARCHAR (5)      NULL,
    [AssayNum]      [dbo].[CrudeNum] NULL,
    [LubeGroup]     VARCHAR (5)      NULL,
    [EUFitGroup]    VARCHAR (6)      NULL,
    CONSTRAINT [PK_Crude_LU_1__13] PRIMARY KEY CLUSTERED ([CNum] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE TRIGGER dbo.Crude_LU_LubeGroup 
   ON  dbo.Crude_LU
   AFTER INSERT,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF UPDATE(CNum)
		UPDATE Crude_LU
		SET LubeGroup = CNum
		WHERE CNum IN (SELECT CNum FROM inserted) AND (LubeGroup IS NULL OR LubeGroup IN (SELECT CNum FROM deleted))

END

GO


CREATE    trigger [dbo].[Crude_LU_GenericCNum] on [dbo].[Crude_LU] for insert, update 
AS
SET NOCOUNT ON;
IF UPDATE(CNum)
	UPDATE Crude_LU
	SET GenericCNum = SubString(CNum,1,4)
	WHERE CNum IN (SELECT CNum FROM inserted)


