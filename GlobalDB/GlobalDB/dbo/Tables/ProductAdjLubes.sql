﻿CREATE TABLE [dbo].[ProductAdjLubes] (
    [StudyYear]      SMALLINT      NOT NULL,
    [PriceLoc]       SMALLINT      NOT NULL,
    [ProductID]      CHAR (5)      NOT NULL,
    [BaseProductID]  CHAR (5)      NULL,
    [AnilineSlope]   REAL          NULL,
    [AnilineSpec]    REAL          NULL,
    [AnilineDefault] REAL          NULL,
    [N2ppmSlope]     REAL          NULL,
    [N2PPMSpec]      REAL          NULL,
    [N2PPMDefault]   REAL          NULL,
    [VABPSlope]      REAL          NULL,
    [VABPSpec]       REAL          NULL,
    [VABPDefault]    REAL          NULL,
    [SulfurSlope]    REAL          NULL,
    [SulfurSpec]     REAL          NULL,
    [SulfurDefault]  REAL          NULL,
    [FuelOilID]      CHAR (5)      NULL,
    [FuelOilPcnt]    REAL          NULL,
    [SaveDate]       SMALLDATETIME NULL,
    CONSTRAINT [PK_ProductAdjLubes] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [PriceLoc] ASC, [ProductID] ASC) WITH (FILLFACTOR = 90)
);

