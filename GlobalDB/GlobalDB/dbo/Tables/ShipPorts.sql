﻿CREATE TABLE [dbo].[ShipPorts] (
    [PortCode] VARCHAR (5)   NOT NULL,
    [PortName] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ShipPorts] PRIMARY KEY CLUSTERED ([PortCode] ASC)
);

