﻿CREATE TABLE [dbo].[SlideColors] (
    [SlideColorID]    INT          NOT NULL,
    [SlideColorGroup] INT          NOT NULL,
    [Color]           VARCHAR (6)  NOT NULL,
    [GradientColor]   VARCHAR (6)  NULL,
    [ColorGroupName]  VARCHAR (50) NULL,
    [ColorOrder]      REAL         NULL
);

