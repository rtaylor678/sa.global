﻿CREATE TABLE [dbo].[ProductPrice] (
    [StudyYear]    SMALLINT      NOT NULL,
    [Period]       CHAR (2)      CONSTRAINT [DF_ProductPrice_Period] DEFAULT ('TY') NOT NULL,
    [PricingBasis] CHAR (4)      NOT NULL,
    [PriceLoc]     SMALLINT      NOT NULL,
    [ProductID]    CHAR (5)      NOT NULL,
    [BasePrice]    REAL          NOT NULL,
    [SaveDate]     SMALLDATETIME CONSTRAINT [DF_ProdPrice_SaveDate] DEFAULT (getdate()) NOT NULL,
    [BasePriceMT]  REAL          NULL,
    CONSTRAINT [PK_ProdPrice_1__13] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [Period] ASC, [PricingBasis] ASC, [PriceLoc] ASC, [ProductID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ProductPrice_PriceLoc_LU] FOREIGN KEY ([PriceLoc]) REFERENCES [dbo].[PriceLoc_LU] ([PriceLoc])
);


GO
/****** Object:  Trigger dbo.UpdateProductPriceDate    Script Date: 11/09/2001 2:28:50 PM ******/
/****** Object:  Trigger dbo.UpdateProductPriceDate    Script Date: 2/10/99 5:38:55 PM ******/
CREATE TRIGGER UpdateProductPriceDate ON dbo.ProductPrice 
FOR UPDATE
AS
UPDATE ProductPrice
SET SaveDate = GetDate()
FROM deleted, ProductPrice
WHERE deleted.StudyYear = ProductPrice.StudyYear
AND deleted.Period = ProductPrice.Period
AND deleted.PricingBasis = ProductPrice.PricingBasis
AND deleted.PriceLoc = ProductPrice.PriceLoc
AND deleted.ProductID = ProductPrice.ProductID
AND deleted.BasePrice <> ProductPrice.BasePrice
