﻿CREATE TABLE [dbo].[ConstEnergyPrices] (
    [EPricingBasis] CHAR (5)           NOT NULL,
    [EnergyType]    [dbo].[EnergyType] NOT NULL,
    [Price]         [dbo].[Price]      NULL,
    CONSTRAINT [PK_ConstEnergyPrices_1__23] PRIMARY KEY CLUSTERED ([EPricingBasis] ASC, [EnergyType] ASC) WITH (FILLFACTOR = 90)
);

