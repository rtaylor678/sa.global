﻿CREATE TABLE [dbo].[CrudePrice] (
    [StudyYear]      [dbo].[StudyYear] NOT NULL,
    [Period]         [dbo].[Period]    NOT NULL,
    [CNum]           CHAR (5)          NOT NULL,
    [BasePrice]      REAL              NOT NULL,
    [APIScale]       TINYINT           NOT NULL,
    [PostedAPI]      REAL              NOT NULL,
    [SaveDate]       SMALLDATETIME     CONSTRAINT [DF_CrudePrice_SaveDate] DEFAULT (getdate()) NOT NULL,
    [CrudeOrigin]    SMALLINT          NULL,
    [PostedSulfur]   REAL              NULL,
    [SulfurAdjSlope] REAL              NULL,
    CONSTRAINT [PK_SAICrude_1__13] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [Period] ASC, [CNum] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CrudePrice_CrudeOrigin_LU] FOREIGN KEY ([CrudeOrigin]) REFERENCES [dbo].[CrudeOrigin_LU] ([CrudeOrigin])
);


GO
/****** Object:  Trigger dbo.UpdateCrudePriceDate    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE TRIGGER UpdateCrudePriceDate ON dbo.CrudePrice 
FOR UPDATE
AS
UPDATE CrudePrice
SET SaveDate = GetDate()
FROM deleted, CrudePrice
WHERE deleted.StudyYear = CrudePrice.StudyYear
AND deleted.Period = CrudePrice.Period
AND deleted.CNum = CrudePrice.CNum
AND (deleted.BasePrice <> CrudePrice.BasePrice
OR deleted.APIScale <> CrudePrice.APIScale
OR deleted.PostedAPI <> CrudePrice.PostedAPI
OR deleted.CrudeOrigin <> CrudePrice.CrudeOrigin)
