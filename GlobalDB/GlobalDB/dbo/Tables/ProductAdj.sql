﻿CREATE TABLE [dbo].[ProductAdj] (
    [StudyYear]    SMALLINT      NOT NULL,
    [Period]       CHAR (2)      CONSTRAINT [DF_ProductAdj_Period] DEFAULT ('TY') NOT NULL,
    [PricingBasis] CHAR (4)      NOT NULL,
    [PriceLoc]     SMALLINT      NOT NULL,
    [ProductID]    CHAR (5)      NOT NULL,
    [Property]     CHAR (5)      NOT NULL,
    [PLevel]       TINYINT       CONSTRAINT [DF_ProductAdj_Level] DEFAULT (1) NOT NULL,
    [MinValue]     REAL          NULL,
    [MaxValue]     REAL          NULL,
    [Adj]          REAL          NULL,
    [SaveDate]     SMALLDATETIME CONSTRAINT [DF_ProdAdj_SaveDate] DEFAULT (getdate()) NOT NULL,
    [AdjMT]        REAL          NULL,
    CONSTRAINT [PK_ProductAdj] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [Period] ASC, [PricingBasis] ASC, [PriceLoc] ASC, [ProductID] ASC, [Property] ASC, [PLevel] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ProductAdj_PriceLoc_LU] FOREIGN KEY ([PriceLoc]) REFERENCES [dbo].[PriceLoc_LU] ([PriceLoc])
);


GO
/****** Object:  Trigger dbo.UpdateProductAdjDate    Script Date: 11/09/2001 2:28:50 PM ******/
/****** Object:  Trigger dbo.UpdateProductAdjDate    Script Date: 2/10/99 5:38:55 PM ******/
CREATE TRIGGER UpdateProductAdjDate ON dbo.ProductAdj 
FOR UPDATE
AS
UPDATE ProductAdj
SET SaveDate = GetDate()
FROM deleted, ProductAdj
WHERE deleted.StudyYear = ProductAdj.StudyYear
AND deleted.Period = ProductAdj.Period
AND deleted.PricingBasis = ProductAdj.PricingBasis
AND deleted.PriceLoc = ProductAdj.PriceLoc
AND deleted.ProductID = ProductAdj.ProductID
AND deleted.Property = ProductAdj.Property
AND deleted.PLevel = ProductAdj.PLevel
AND (deleted.MinValue <> ProductAdj.MinValue
	OR deleted.MaxValue <> ProductAdj.MaxValue
	OR deleted.Adj <> ProductAdj.Adj)
