﻿CREATE TABLE [dbo].[MaterialSumCat_LU] (
    [SumCat]   CHAR (4)     NOT NULL,
    [Category] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_MaterialSumCat_LU] PRIMARY KEY CLUSTERED ([SumCat] ASC) WITH (FILLFACTOR = 90)
);

