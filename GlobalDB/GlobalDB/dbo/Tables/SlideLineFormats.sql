﻿CREATE TABLE [dbo].[SlideLineFormats] (
    [SlideLineFormatID] INT  NOT NULL,
    [LineColor]         INT  NULL,
    [LineWidth]         REAL NULL,
    [LineDashStyle]     INT  NULL,
    [LineBeginArrow]    BIT  NULL,
    [LineEndArrow]      BIT  NULL,
    [MarkerType]        INT  NULL,
    [MarkerSize]        INT  NULL,
    [MarkerLastOnly]    BIT  NULL
);

