﻿CREATE TABLE [dbo].[CrudePriceAlt] (
    [StudyYear] [dbo].[StudyYear] NOT NULL,
    [Scenario]  [dbo].[Scenario]  NOT NULL,
    [Period]    [dbo].[Period]    NOT NULL,
    [CNum]      CHAR (5)          NOT NULL,
    [BasePrice] REAL              NOT NULL,
    [SaveDate]  SMALLDATETIME     CONSTRAINT [DF_CrudePriceAlt_SaveDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CrudePriceAlt] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [Scenario] ASC, [Period] ASC, [CNum] ASC) WITH (FILLFACTOR = 90)
);

