﻿CREATE TABLE [dbo].[Transportation] (
    [StudyYear]   SMALLINT      NOT NULL,
    [Period]      CHAR (2)      NOT NULL,
    [CrudeOrigin] SMALLINT      NOT NULL,
    [CrudeDest]   SMALLINT      NOT NULL,
    [AdjType]     CHAR (2)      NOT NULL,
    [AdjValue]    REAL          NULL,
    [SaveDate]    SMALLDATETIME CONSTRAINT [DF_Trans_SaveDate_3__13] DEFAULT (getdate()) NOT NULL,
    [AdjValueMT]  REAL          NULL,
    CONSTRAINT [PK_Trans_2__13] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [Period] ASC, [CrudeOrigin] ASC, [CrudeDest] ASC, [AdjType] ASC) WITH (FILLFACTOR = 90)
);


GO
/****** Object:  Trigger dbo.UpdateTransDate    Script Date: 11/09/2001 2:28:50 PM ******/
/****** Object:  Trigger dbo.UpdateTransDate    Script Date: 3/6/2001 9:48:05 AM ******/
/****** Object:  Trigger dbo.UpdateTransDate    Script Date: 2/10/99 5:38:55 PM ******/
CREATE TRIGGER UpdateTransDate ON dbo.Transportation 
FOR UPDATE 
AS
UPDATE Transportation
SET SaveDate = GetDate()
FROM deleted, Transportation
WHERE deleted.StudyYear = Transportation.StudyYear
AND deleted.Period = Transportation.Period
AND deleted.CrudeOrigin = Transportation.CrudeOrigin
AND deleted.CrudeDest = Transportation.CrudeDest
AND deleted.AdjType = Transportation.AdjType
AND (deleted.AdjValue <> Transportation.AdjValue OR 
     deleted.AdjValueMT <> Transportation.AdjValueMT)
