﻿CREATE TABLE [dbo].[StudyGroups] (
    [StudyGroup]  CHAR (5)  NOT NULL,
    [Description] CHAR (30) NOT NULL,
    CONSTRAINT [PK__StudyGroups__22AA2996] PRIMARY KEY CLUSTERED ([StudyGroup] ASC) WITH (FILLFACTOR = 90)
);

