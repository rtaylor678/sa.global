﻿CREATE TABLE [dbo].[APIScale_LU] (
    [APIScale]    TINYINT   NOT NULL,
    [Description] CHAR (25) NULL,
    CONSTRAINT [PK_APIScale_LU_1__14] PRIMARY KEY CLUSTERED ([APIScale] ASC) WITH (FILLFACTOR = 90)
);

