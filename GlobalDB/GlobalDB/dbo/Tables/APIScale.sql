﻿CREATE TABLE [dbo].[APIScale] (
    [StudyYear] [dbo].[StudyYear] NOT NULL,
    [APIScale]  TINYINT           NOT NULL,
    [APILevel]  TINYINT           NOT NULL,
    [MinAPI]    REAL              NOT NULL,
    [MaxAPI]    REAL              NOT NULL,
    [Adj]       [dbo].[Price]     NOT NULL,
    [SaveDate]  SMALLDATETIME     CONSTRAINT [DF_APIScale_SaveDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Scale_1__17] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [APIScale] ASC, [APILevel] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_APIScale_APIScale_LU] FOREIGN KEY ([APIScale]) REFERENCES [dbo].[APIScale_LU] ([APIScale])
);


GO
/****** Object:  Trigger dbo.UpdateAPIScaleDate    Script Date: 2/10/99 5:38:54 PM ******/
CREATE TRIGGER UpdateAPIScaleDate ON dbo.APIScale 
FOR UPDATE
AS
UPDATE APIScale
SET SaveDate = GetDate()
FROM deleted, APIScale
WHERE deleted.StudyYear = APIScale.StudyYear
	AND deleted.APIScale = APIScale.APIScale
	AND deleted.APILevel = APIScale.APILevel
	AND (deleted.MinAPI <> APIScale.MinAPI
	OR deleted.MaxAPI <> APIScale.MaxAPI
	OR deleted.Adj <> APIScale.Adj)
