﻿CREATE PROCEDURE [dbo].[GetBaseProductPrice](@StudyYear smallint, @Period char(2), 
	@PricingBasis char(4), @PriceLoc smallint, @ProductID char(5), @Density real = NULL,
	@BasePrice real OUTPUT, @BasePriceMT real OUTPUT)
AS
SELECT @BasePrice = NULL, @BasePriceMT = NULL
SELECT @BasePrice = BasePrice, @BasePriceMT = BasePriceMT
FROM ProductPrice
WHERE StudyYear = @StudyYear AND Period = @Period 
AND PricingBasis = @PricingBasis AND PriceLoc = @PriceLoc
AND ProductID = @ProductID

IF @Density IS NOT NULL AND @BasePriceMT IS NOT NULL
	SELECT @BasePrice = @BasePriceMT*@Density/6289.7

