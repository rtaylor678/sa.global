﻿
CREATE  PROCEDURE GetResidPrice(@StudyYear smallint, @Period char(2), 
	@PricingBasis char(4), @PriceLoc smallint, @MaterialID char(5), 
	@Viscosity real, @Sulfur real, @Density real = NULL,
	@BasePrice real OUTPUT, @BasePriceMT real OUTPUT,
	@ViscosityAdj real OUTPUT, @ViscosityAdjMT real OUTPUT,
	@SulfurAdj real OUTPUT, @SulfurAdjMT real OUTPUT,
	@PricePerBbl real OUTPUT, @PricePerMT real OUTPUT)
AS
DECLARE @ProductID char(5)
SELECT @ProductID = PricingID FROM Material_LU WHERE MaterialID = @MaterialID

EXEC GetBaseProductPrice @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Density, @BasePrice OUTPUT, @BasePriceMT OUTPUT
EXEC GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'VISC', @Viscosity, @Density, @ViscosityAdj OUTPUT, @ViscosityAdjMT OUTPUT
EXEC GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'SULF', @Sulfur, @Density, @SulfurAdj OUTPUT, @SulfurAdjMT OUTPUT
SELECT @PricePerBbl = @BasePrice + ISNULL(@ViscosityAdj, 0) + ISNULL(@SulfurAdj, 0),
	@PricePerMT = @BasePriceMT + ISNULL(@ViscosityAdjMT, 0) + ISNULL(@SulfurAdjMT, 0)


