﻿
CREATE    PROCEDURE CheckProductAdjLubes;1 (
	@StudyYear smallint, @PriceLoc smallint, @ProductID varchar(5), @BaseProductID char(5) = NULL,
	@AnilineSlope real = NULL, @AnilineSpec real = NULL, @AnilineDefault real = NULL, 
	@N2PPMSlope real = NULL, @N2PPMSpec real = NULL, @N2PPMDefault real = NULL, 
	@VABPSlope real = NULL, @VABPSpec real = NULL, @VABPDefault real = NULL, 
	@SulfurSlope real = NULL, @SulfurSpec real = NULL, @SulfurDefault real = NULL, 
	@FuelOilPcnt real = NULL, @FuelOilID char(5) = NULL)
AS
DECLARE @sAnilineSlope char(1), @sAnilineSpec char(1), @sAnilineDefault char(1), 
	@sN2PPMSlope char(1), @sN2PPMSpec char(1), @sN2PPMDefault char(1), 
	@sVABPSlope char(1), @sVABPSpec char(1), @sVABPDefault char(1), 
	@sSulfurSlope char(1), @sSulfurSpec char(1), @sSulfurDefault char(1), 
	@sFuelOilPcnt char(1), @sFuelOilID char(1), @sBaseProductID char(1),
	@NewAdj tinyint

EXEC CheckProductAdjLubes;2 @StudyYear, @PriceLoc, @ProductID, @BaseProductID OUTPUT,
	@AnilineSlope OUTPUT, @AnilineSpec OUTPUT,  @AnilineDefault OUTPUT, 
	@N2PPMSlope OUTPUT, @N2PPMSpec OUTPUT,  @N2PPMDefault OUTPUT, 
	@VABPSlope OUTPUT, @VABPSpec OUTPUT,  @VABPDefault OUTPUT, 
	@SulfurSlope OUTPUT, @SulfurSpec OUTPUT, @SulfurDefault OUTPUT, 
	@FuelOilPcnt OUTPUT, @FuelOilID OUTPUT,
	@sAnilineSlope OUTPUT, @sAnilineSpec OUTPUT, @sAnilineDefault OUTPUT, 
	@sN2PPMSlope OUTPUT, @sN2PPMSpec OUTPUT, @sN2PPMDefault OUTPUT, 
	@sVABPSlope OUTPUT, @sVABPSpec OUTPUT, @sVABPDefault OUTPUT, 
	@sSulfurSlope OUTPUT, @sSulfurSpec OUTPUT, @sSulfurDefault OUTPUT, 
	@sFuelOilPcnt OUTPUT, @sFuelOilID OUTPUT, @sBaseProductID OUTPUT, @NewAdj OUTPUT

EXEC CheckProductAdjLubes;3 @BaseProductID, 
	@AnilineSlope, @AnilineSpec, @AnilineDefault,
	@N2PPMSlope, @N2PPMSpec, @N2PPMDefault,
	@VABPSlope, @VABPSpec, @VABPDefault,
	@SulfurSlope, @SulfurSpec, @SulfurDefault,
	@FuelOilPcnt, @FuelOilID,
	@sAnilineSlope, @sAnilineSpec, @sAnilineDefault,
	@sN2PPMSlope, @sN2PPMSpec, @sN2PPMDefault,
	@sVABPSlope, @sVABPSpec, @sVABPDefault,
	@sSulfurSlope, @sSulfurSpec, @sSulfurDefault,
	@sFuelOilPcnt, @sFuelOilID, @sBaseProductID, @NewAdj


