﻿CREATE PROCEDURE CheckTransportation;2 (
	@StudyYear smallint, @Period char(2), 
	@CrudeOrigin smallint, @CrudeDest smallint, 
	@Duty real = NULL OUTPUT, @Facilities real = NULL OUTPUT, @Gathering real = NULL OUTPUT,
	@Insurance real = NULL OUTPUT, @Losses real = NULL OUTPUT, @Lightering real = NULL OUTPUT, 
	@MiscPcnt real = NULL OUTPUT, @MiscAdd real = NULL OUTPUT, @MarketAdj real = NULL OUTPUT, 
	@OceanFreight real = NULL OUTPUT, @OceanFreightMT real = NULL OUTPUT, 
	@OPAInsurance real = NULL OUTPUT, @Port real = NULL OUTPUT, 
	@Pipeline real = NULL OUTPUT, @Superfund real = NULL OUTPUT,
	@sDuty char(1) OUTPUT, @sFacilities char(1) OUTPUT, @sGathering char(1) OUTPUT,
	@sInsurance char(1) OUTPUT, @sLosses char(1) OUTPUT, @sLightering char(1) OUTPUT, 
	@sMiscPcnt char(1) OUTPUT, @sMiscAdd char(1) OUTPUT, @sMarketAdj char(1) OUTPUT, 
	@sOceanFreight char(1) OUTPUT, @sOPAInsurance char(1) OUTPUT, 
	@sPort char(1) OUTPUT, @sPipeline char(1) OUTPUT, @sSuperfund char(1) OUTPUT)
AS
DECLARE	@CurrDuty real,  @CurrFacilities real, @CurrGathering real,
	@CurrInsurance real, @CurrLosses real, @CurrLightering real, 
	@CurrMiscPcnt real, @CurrMiscAdd real, @CurrMarketAdj real, 
	@CurrOceanFreight real, @CurrOceanFreightMT real, @CurrOPAInsurance real,
	@CurrPort real, @CurrPipeline real, @CurrSuperfund real
SELECT @CurrDuty = SUM(CASE WHEN AdjType = 'D' THEN AdjValue END),
@CurrFacilities = SUM(CASE WHEN AdjType = 'FF' THEN AdjValue END),
@CurrGathering = SUM(CASE WHEN AdjType = 'GA' THEN AdjValue END),
@CurrInsurance = SUM(CASE WHEN AdjType = 'IN' THEN AdjValue END),
@CurrLosses = SUM(CASE WHEN AdjType = 'L' THEN AdjValue END),
@CurrLightering = SUM(CASE WHEN AdjType = 'LI' THEN AdjValue END),
@CurrMiscPcnt = SUM(CASE WHEN AdjType = 'M%' THEN AdjValue END),
@CurrMiscAdd = SUM(CASE WHEN AdjType = 'M+' THEN AdjValue END),
@CurrMarketAdj = SUM(CASE WHEN AdjType = 'MA' THEN AdjValue END),
@CurrOceanFreight = SUM(CASE WHEN AdjType = 'OF' THEN AdjValue END),
@CurrOceanFreightMT = SUM(CASE WHEN AdjType = 'OF' THEN AdjValueMT END),
@CurrOPAInsurance = SUM(CASE WHEN AdjType = 'OI' THEN AdjValue END),
@CurrPort = SUM(CASE WHEN AdjType = 'PF' THEN AdjValue END),
@CurrPipeline = SUM(CASE WHEN AdjType = 'PT' THEN AdjValue END),
@CurrSuperfund = SUM(CASE WHEN AdjType = 'SF' THEN AdjValue END)
FROM Transportation
WHERE StudyYear = @StudyYear AND Period = @Period
AND CrudeOrigin = @CrudeOrigin AND CrudeDest = @CrudeDest
IF @CurrDuty IS NULL AND @Duty IS NOT NULL
	SELECT @sDuty = 'I'
ELSE IF @CurrDuty IS NOT NULL AND @Duty IS NULL
	SELECT @sDuty = 'D'
ELSE IF ABS(@CurrDuty - @Duty) > 0.001
	SELECT @sDuty = 'U'
ELSE
	SELECT @sDuty = 'N'
IF @CurrFacilities IS NULL AND @Facilities IS NOT NULL
	SELECT @sFacilities = 'I'
ELSE IF @CurrFacilities IS NOT NULL AND @Facilities IS NULL
	SELECT @sFacilities = 'D'
ELSE IF ABS(@CurrFacilities - @Facilities) > 0.001
	SELECT @sFacilities = 'U'
ELSE
	SELECT @sFacilities = 'N'
IF @CurrGathering IS NULL AND @Gathering IS NOT NULL
	SELECT @sGathering = 'I'
ELSE IF @CurrGathering IS NOT NULL AND @Gathering IS NULL
	SELECT @sGathering = 'D'
ELSE IF ABS(@CurrGathering - @Gathering) > 0.001
	SELECT @sGathering = 'U'
ELSE
	SELECT @sGathering = 'N'
IF @CurrInsurance IS NULL AND @Insurance IS NOT NULL
	SELECT @sInsurance = 'I'
ELSE IF @CurrInsurance IS NOT NULL AND @Insurance IS NULL
	SELECT @sInsurance = 'D'
ELSE IF ABS(@CurrInsurance - @Insurance) > 0.001
	SELECT @sInsurance = 'U'
ELSE
	SELECT @sInsurance = 'N'
IF @CurrLosses IS NULL AND @Losses IS NOT NULL
	SELECT @sLosses = 'I'
ELSE IF @CurrLosses IS NOT NULL AND @Losses IS NULL
	SELECT @sLosses = 'D'
ELSE IF ABS(@CurrLosses - @Losses) > 0.001
	SELECT @sLosses = 'U'
ELSE
	SELECT @sLosses = 'N'
IF @CurrLightering IS NULL AND @Lightering IS NOT NULL
	SELECT @sLightering = 'I'
ELSE IF @CurrLightering IS NOT NULL AND @Lightering IS NULL
	SELECT @sLightering = 'D'
ELSE IF ABS(@CurrLightering - @Lightering) > 0.001
	SELECT @sLightering = 'U'
ELSE
	SELECT @sLightering = 'N'
IF @CurrMiscPcnt IS NULL AND @MiscPcnt IS NOT NULL
	SELECT @sMiscPcnt = 'I'
ELSE IF @CurrMiscPcnt IS NOT NULL AND @MiscPcnt IS NULL
	SELECT @sMiscPcnt = 'D'
ELSE IF ABS(@CurrMiscPcnt - @MiscPcnt) > 0.001
	SELECT @sMiscPcnt = 'U'
ELSE
	SELECT @sMiscPcnt = 'N'
IF @CurrMiscAdd IS NULL AND @MiscAdd IS NOT NULL
	SELECT @sMiscAdd = 'I'
ELSE IF @CurrMiscAdd IS NOT NULL AND @MiscAdd IS NULL
	SELECT @sMiscAdd = 'D'
ELSE IF ABS(@CurrMiscAdd - @MiscAdd) > 0.001
	SELECT @sMiscAdd = 'U'
ELSE
	SELECT @sMiscAdd = 'N'
IF @CurrMarketAdj IS NULL AND @MarketAdj IS NOT NULL
	SELECT @sMarketAdj = 'I'
ELSE IF @CurrMarketAdj IS NOT NULL AND @MarketAdj IS NULL
	SELECT @sMarketAdj = 'D'
ELSE IF ABS(@CurrMarketAdj - @MarketAdj) > 0.001
	SELECT @sMarketAdj = 'U'
ELSE
	SELECT @sMarketAdj = 'N'
SELECT @sOceanFreight = 'N'
IF (@CurrOceanFreight IS NULL AND @CurrOceanFreightMT IS NULL) AND 
   (@OceanFreight IS NOT NULL OR @OceanFreightMT IS NOT NULL)
	SELECT @sOceanFreight = 'I'
ELSE IF (@CurrOceanFreight IS NOT NULL OR @CurrOceanFreightMT IS NOT NULL) AND 
	(@OceanFreight IS NULL AND @OceanFreightMT IS NULL)
	SELECT @sOceanFreight = 'D'
ELSE IF ABS(ISNULL(@CurrOceanFreight,0) - ISNULL(@OceanFreight,0)) > 0.001
	SELECT @sOceanFreight = 'U'
ELSE IF ABS(ISNULL(@CurrOceanFreightMT,0) - ISNULL(@OceanFreightMT,0)) > 0.001
	SELECT @sOceanFreight = 'U'
IF @CurrOPAInsurance IS NULL AND @OPAInsurance IS NOT NULL
	SELECT @sOPAInsurance = 'I'
ELSE IF @CurrOPAInsurance IS NOT NULL AND @OPAInsurance IS NULL
	SELECT @sOPAInsurance = 'D'
ELSE IF ABS(@CurrOPAInsurance - @OPAInsurance) > 0.001
	SELECT @sOPAInsurance = 'U'
ELSE
	SELECT @sOPAInsurance = 'N'
IF @CurrPort IS NULL AND @Port IS NOT NULL
	SELECT @sPort = 'I'
ELSE IF @CurrPort IS NOT NULL AND @Port IS NULL
	SELECT @sPort = 'D'
ELSE IF ABS(@CurrPort - @Port) > 0.001
	SELECT @sPort = 'U'
ELSE
	SELECT @sPort = 'N'
IF @CurrPipeline IS NULL AND @Pipeline IS NOT NULL
	SELECT @sPipeline = 'I'
ELSE IF @CurrPipeline IS NOT NULL AND @Pipeline IS NULL
	SELECT @sPipeline = 'D'
ELSE IF ABS(@CurrPipeline - @Pipeline) > 0.001
	SELECT @sPipeline = 'U'
ELSE
	SELECT @sPipeline = 'N'
IF @CurrSuperfund IS NULL AND @Superfund IS NOT NULL
	SELECT @sSuperfund = 'I'
ELSE IF @CurrSuperfund IS NOT NULL AND @Superfund IS NULL
	SELECT @sSuperfund = 'D'
ELSE IF ABS(@CurrSuperfund - @Superfund) > 0.001
	SELECT @sSuperfund = 'U'
ELSE
	SELECT @sSuperfund = 'N'
SELECT  @Duty = @CurrDuty, @Facilities = @CurrFacilities, 
	@Gathering = @CurrGathering, @Insurance = @CurrInsurance, 
	@Losses = @CurrLosses, @Lightering = @CurrLightering, 
	@MiscPcnt = @CurrMiscPcnt, @MiscAdd = @CurrMiscAdd, @MarketAdj = @CurrMarketAdj, 
	@OceanFreight = @CurrOceanFreight, @OceanFreightMT = @CurrOceanFreightMT, 
	@OPAInsurance = @CurrOPAInsurance, @Port = @CurrPort, 
	@Pipeline = @CurrPipeline, @Superfund = @CurrSuperfund