﻿CREATE PROCEDURE GravityConv (@API real OUTPUT, @Density real OUTPUT) 
AS
IF @API = NULL
	SELECT @API = (1000*141.5/@Density)-131.5
ELSE
	SELECT @Density = 1000*(141.5/(@API+131.5))
