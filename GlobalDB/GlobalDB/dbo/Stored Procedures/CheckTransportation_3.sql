﻿CREATE PROCEDURE CheckTransportation;3 (
	@Duty real = NULL, @Facilities real = NULL, @Gathering real = NULL,
	@Insurance real = NULL, @Losses real = NULL, @Lightering real = NULL, 
	@MiscPcnt real = NULL, @MiscAdd real = NULL, @MarketAdj real = NULL, 
	@OceanFreight real = NULL, @OceanFreightMT real = NULL, @OPAInsurance real = NULL,
	@Port real = NULL, @Pipeline real = NULL, @Superfund real = NULL,
	@sDuty char(1) = NULL, @sFacilities char(1) = NULL, @sGathering char(1) = NULL,
	@sInsurance char(1) = NULL, @sLosses char(1) = NULL, @sLightering char(1) = NULL, 
	@sMiscPcnt char(1) = NULL, @sMiscAdd char(1) = NULL, @sMarketAdj char(1) = NULL, 
	@sOceanFreight char(1) = NULL, 
	@sOPAInsurance char(1) = NULL, @sPort char(1) = NULL, 
	@sPipeline char(1) = NULL, @sSuperfund char(1) = NULL)
AS
SELECT Action = @sDuty, AdjType = 'D', AdjValue = @Duty, AdjValueMT = NULL
WHERE @sDuty <> 'N'
UNION
SELECT @sFacilities, 'FF', @Facilities, NULL
WHERE @sFacilities <> 'N'
UNION
SELECT @sGathering, 'GA', @Gathering, NULL
WHERE @sGathering <> 'N'
UNION
SELECT @sInsurance, 'IN', @Insurance, NULL
WHERE @sInsurance <> 'N'
UNION
SELECT @sLosses, 'L', @Losses, NULL
WHERE @sLosses <> 'N'
UNION
SELECT @sLightering, 'LI', @Lightering, NULL
WHERE @sLightering <> 'N'
UNION
SELECT @sMiscPcnt, 'M%', @MiscPcnt, NULL
WHERE @sMiscPcnt <> 'N'
UNION
SELECT @sMiscAdd, 'M+', @MiscAdd, NULL
WHERE @sMiscAdd <> 'N'
UNION
SELECT @sMarketAdj, 'MA', @MarketAdj, NULL
WHERE @sMarketAdj <> 'N'
UNION
SELECT @sOceanFreight, 'OF', @OceanFreight, @OceanFreightMT
WHERE @sOceanFreight <> 'N'
UNION
SELECT @sOPAInsurance, 'OI', @OPAInsurance, NULL
WHERE @sOPAInsurance <> 'N'
UNION
SELECT @sPort, 'PF', @Port, NULL
WHERE @sPort <> 'N'
UNION
SELECT @sPipeline, 'PT', @Pipeline, NULL
WHERE @sPipeline <> 'N'
UNION
SELECT @sSuperfund, 'SF', @Superfund, NULL
WHERE @sSuperfund <> 'N'