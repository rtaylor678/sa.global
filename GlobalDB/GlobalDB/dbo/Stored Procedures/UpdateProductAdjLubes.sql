﻿


/****** Object:  Stored Procedure dbo.UpdateProductAdjLubes    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE   PROCEDURE UpdateProductAdjLubes (
	@StudyYear smallint, @PriceLoc smallint, @ProductID varchar(5), @BaseProductID char(5) = NULL,
	@AnilineSlope real = NULL, @AnilineSpec real = NULL, @AnilineDefault real = NULL, 
	@N2PPMSlope real = NULL, @N2PPMSpec real = NULL, @N2PPMDefault real = NULL, 
	@VABPSlope real = NULL, @VABPSpec real = NULL, @VABPDefault real = NULL, 
	@SulfurSlope real = NULL, @SulfurSpec real = NULL, @SulfurDefault real = NULL, 
	@FuelOilPcnt real = NULL, @FuelOilID char(5) = NULL)
AS
DECLARE @CurrBaseProductID char(5),
	@CurrAnilineSlope real, @CurrAnilineSpec real, @CurrAnilineDefault real,
	@CurrN2PPMSlope real, @CurrN2PPMSpec real,  @CurrN2PPMDefault real,
	@CurrVABPSlope real, @CurrVABPSpec real,  @CurrVABPDefault real,
	@CurrSulfurSlope real, @CurrSulfurSpec real,  @CurrSulfurDefault real,
	@CurrFuelOilPcnt real, @CurrFuelOilID char(5)
DECLARE @sAnilineSlope char(1), @sAnilineSpec char(1), @sAnilineDefault char(1), 
	@sN2PPMSlope char(1), @sN2PPMSpec char(1), @sN2PPMDefault char(1), 
	@sVABPSlope char(1), @sVABPSpec char(1), @sVABPDefault char(1), 
	@sSulfurSlope char(1), @sSulfurSpec char(1), @sSulfurDefault char(1), 
	@sFuelOilPcnt char(1), @sFuelOilID char(1), @sBaseProductID char(1),
	@NewAdj tinyint
SET NOCOUNT ON
SELECT @CurrBaseProductID = @BaseProductID, 
	@CurrAnilineSlope = @AnilineSlope, @CurrAnilineSpec = @AnilineSpec, @CurrAnilineDefault = @AnilineDefault,
	@CurrN2PPMSlope = @N2PPMSlope, @CurrN2PPMSpec = @N2PPMSpec, @CurrN2PPMDefault = @N2PPMDefault,
	@CurrVABPSlope = @VABPSlope, @CurrVABPSpec = @VABPSpec, @CurrVABPDefault = @VABPDefault,
	@CurrSulfurSlope = @SulfurSlope, @CurrSulfurSpec = @SulfurSpec, @CurrSulfurDefault = @SulfurDefault,
	@CurrFuelOilPcnt = @FuelOilPcnt, @CurrFuelOilID = @FuelOilID
EXEC CheckProductAdjLubes;2 @StudyYear, @PriceLoc, @ProductID, @CurrBaseProductID OUTPUT,
	@CurrAnilineSlope OUTPUT, @CurrAnilineSpec OUTPUT,  @CurrAnilineDefault OUTPUT, 
	@CurrN2PPMSlope OUTPUT, @CurrN2PPMSpec OUTPUT,  @CurrN2PPMDefault OUTPUT, 
	@CurrVABPSlope OUTPUT, @CurrVABPSpec OUTPUT,  @CurrVABPDefault OUTPUT, 
	@CurrSulfurSlope OUTPUT, @CurrSulfurSpec OUTPUT,  @CurrSulfurDefault OUTPUT, 
	@CurrFuelOilPcnt OUTPUT, @CurrFuelOilID OUTPUT,
	@sAnilineSlope OUTPUT, @sAnilineSpec OUTPUT, @sAnilineDefault OUTPUT, 
	@sN2PPMSlope OUTPUT, @sN2PPMSpec OUTPUT, @sN2PPMDefault OUTPUT, 
	@sVABPSlope OUTPUT, @sVABPSpec OUTPUT, @sVABPDefault OUTPUT, 
	@sSulfurSlope OUTPUT, @sSulfurSpec OUTPUT, @sSulfurDefault OUTPUT, 
	@sFuelOilPcnt OUTPUT, @sFuelOilID OUTPUT, @sBaseProductID OUTPUT, @NewAdj OUTPUT
IF @NewAdj = 1
	INSERT INTO dbo.ProductAdjLubes(StudyYear, PriceLoc, ProductID, BaseProductID, 
		AnilineSlope, AnilineSpec, AnilineDefault, N2ppmSlope, N2PPMSpec,  N2PPMDefault,
		VABPSlope, VABPSpec, VABPDefault, SulfurSlope, SulfurSpec, SulfurDefault, FuelOilID, FuelOilPcnt)
	VALUES (@StudyYear, @PriceLoc, @ProductID, @BaseProductID, 
		@AnilineSlope, @AnilineSpec, @AnilineDefault, @N2ppmSlope, @N2PPMSpec, @N2PPMDefault,
		@VABPSlope, @VABPSpec, @VABPDefault, @SulfurSlope, @SulfurSpec, @SulfurDefault, @FuelOilID, @FuelOilPcnt)
ELSE IF @sBaseProductID = 'U' OR @sAnilineSlope = 'U' OR @sAnilineSpec = 'U' OR @sAnilineDefault = 'U' 
	OR @sN2PPMSlope = 'U' OR @sN2PPMSpec = 'U' OR @sN2PPMDefault = 'U' 
	OR @sVABPSlope = 'U' OR @sVABPSpec = 'U' OR @sVABPDefault = 'U' 
	OR @sSulfurSlope = 'U' OR @sSulfurSpec ='U' OR @sSulfurDefault = 'U' 
	OR @sFuelOilPcnt = 'U' OR @sFuelOilID = 'U' OR @sBaseProductID = 'U'

	UPDATE ProductAdjLubes
	SET 	BaseProductID = @BaseProductID, 
		AnilineSlope = @AnilineSlope, AnilineSpec = @AnilineSpec, AnilineDefault = @AnilineDefault,
		N2ppmSlope = @N2ppmSlope, N2PPMSpec = @N2PPMSpec, N2PPMDefault = @N2PPMDefault,
		VABPSlope = @VABPSlope, VABPSpec = @VABPSpec, VABPDefault = @VABPDefault, 
		SulfurSlope = @SulfurSlope, SulfurSpec = @SulfurSpec, SulfurDefault = @SulfurDefault, 
		FuelOilID = @FuelOilID, FuelOilPcnt = @FuelOilPcnt, SaveDate = GetDate()
	WHERE StudyYear = @StudyYear AND PriceLoc = @PriceLoc AND ProductID = @ProductID

EXEC CheckProductAdjLubes;3 @CurrBaseProductID, 
	@CurrAnilineSlope, @CurrAnilineSpec, @CurrAnilineDefault,
	@CurrN2PPMSlope, @CurrN2PPMSpec, @CurrN2PPMDefault,
	@CurrVABPSlope, @CurrVABPSpec, @CurrVABPDefault,
	@CurrSulfurSlope, @CurrSulfurSpec, @CurrSulfurDefault,
	@CurrFuelOilPcnt, @CurrFuelOilID,
	@sAnilineSlope, @sAnilineSpec, @sAnilineDefault,
	@sN2PPMSlope, @sN2PPMSpec, @sN2PPMDefault,
	@sVABPSlope, @sVABPSpec, @sVABPDefault,
	@sSulfurSlope, @sSulfurSpec, @sSulfurDefault,
	@sFuelOilPcnt, @sFuelOilID, @sBaseProductID, @NewAdj



