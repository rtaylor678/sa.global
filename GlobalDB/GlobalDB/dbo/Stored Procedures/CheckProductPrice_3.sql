﻿CREATE PROCEDURE [dbo].[CheckProductPrice];3 (@BasePrice real, @BasePriceMT real = NULL, @NewPrice tinyint)
AS
SELECT NewPrice = @NewPrice, BasePrice = @BasePrice, BasePriceMT = @BasePriceMT
