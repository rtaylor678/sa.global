﻿
/****** Object:  Stored Procedure dbo.UpdateCrudePrice    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE PROCEDURE UpdateCrudePrice (
	@StudyYear smallint, @Period varchar(2), @CNum varchar(5), 
	@BasePrice real, @APIScale smallint, @PostedAPI real, @CrudeOrigin smallint,
	@PostedSulfur real = NULL, @SulfurAdjSlope real = NULL)
AS
DECLARE @CurrBasePrice real, @CurrAPIScale smallint, @CurrPostedAPI real
DECLARE @NewPrice tinyint, @CurrOrigin smallint
DECLARE @CurrPostedSulfur real, @CurrSulfurAdjSlope real
SET NOCOUNT ON
SELECT @CurrBasePrice = @BasePrice, @CurrAPIScale = @APIScale, 
	@CurrPostedAPI = @PostedAPI, @CurrOrigin = @CrudeOrigin,
	@CurrPostedSulfur = @PostedSulfur, @CurrSulfurAdjSlope = @SulfurAdjSlope
EXEC CheckCrudePrice;2 @StudyYear, @Period, @CNum, @CurrBasePrice OUTPUT, 
	@CurrAPIScale OUTPUT, @CurrPostedAPI OUTPUT, @CurrOrigin OUTPUT, 
	@NewPrice OUTPUT, @CurrPostedSulfur OUTPUT, @CurrSulfurAdjSlope OUTPUT
IF @NewPrice = 1
	INSERT INTO CrudePrice (StudyYear, Period, CNum, PostedAPI, 
		APIScale, BasePrice, SaveDate, CrudeOrigin, PostedSulfur, SulfurAdjSlope)
	VALUES (@StudyYear, @Period, @CNum, @PostedAPI, 
		@APIScale, @BasePrice, GetDate(), @CrudeOrigin, @PostedSulfur, @SulfurAdjSlope)
ELSE
	IF @CurrPostedAPI IS NOT NULL OR @CurrAPIScale IS NOT NULL
	OR @CurrBasePrice IS NOT NULL OR @CurrOrigin Is NOT NULL
	OR @CurrPostedSulfur IS NOT NULL OR @CurrSulfurAdjSlope IS NOT NULL
		UPDATE CrudePrice
		SET PostedAPI = @PostedAPI, APIScale = @APIScale,
		BasePrice = @BasePrice, CrudeOrigin = @CrudeOrigin, 
		PostedSulfur = @PostedSulfur, SulfurAdjSlope = @SulfurAdjSlope,
		SaveDate = GetDate()
		WHERE StudyYear = @StudyYear AND Period = @Period
		AND CNum = @CNum
EXEC CheckCrudePrice;3 @CurrBasePrice, @CurrAPIScale, @CurrPostedAPI, 
	@CurrOrigin, @NewPrice, @CurrPostedSulfur, @CurrSulfurAdjSlope

