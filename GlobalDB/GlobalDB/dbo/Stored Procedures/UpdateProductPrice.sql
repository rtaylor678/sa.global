﻿/****** Object:  Stored Procedure dbo.UpdateProductPrice    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE PROCEDURE [dbo].[UpdateProductPrice] (
	@StudyYear smallint, @Period varchar(2) = 'TY', 
	@PricingBasis char(4) = 'BASE', @PriceLoc smallint, 
	@ProductID char(5), @BasePrice real = NULL, @BasePriceMT real = NULL)
AS
DECLARE @CurrBasePrice real, @CurrBasePriceMT real, @NewPrice tinyint
SET NOCOUNT ON
SELECT @CurrBasePrice = @BasePrice, @CurrBasePriceMT = @BasePriceMT
EXEC CheckProductPrice;2 @StudyYear, @Period, @PricingBasis, @PriceLoc, 
	@ProductID, @CurrBasePrice OUTPUT, @CurrBasePriceMT OUTPUT, 
	@NewPrice OUTPUT
IF @BasePrice IS NULL AND @BasePriceMT IS NOT NULL
BEGIN
	IF @BasePriceMT = 0
		SET @BasePrice = 0
	ELSE
		EXEC ConvProductMTPrice @ProductID, @BasePriceMT, @BasePrice OUTPUT
END
IF @NewPrice = 1
	INSERT INTO ProductPrice (StudyYear, Period, PricingBasis,
		PriceLoc, ProductID, BasePrice, BasePriceMT, SaveDate)
	VALUES (@StudyYear, @Period, @PricingBasis, 
		@PriceLoc, @ProductID, @BasePrice, @BasePriceMT, GetDate())
ELSE IF @CurrBasePrice IS NOT NULL AND @BasePrice IS NOT NULL
	UPDATE ProductPrice
	SET BasePrice = @BasePrice, BasePriceMT = @BasePriceMT,
	SaveDate = GetDate()
	WHERE StudyYear = @StudyYear AND Period = @Period
	AND PricingBasis = @PricingBasis
	AND PriceLoc = @PriceLoc
	AND ProductID = @ProductID
ELSE IF @CurrBasePrice IS NOT NULL AND @BasePrice IS NULL
	DELETE ProductPrice
	WHERE StudyYear = @StudyYear AND Period = @Period
	AND PricingBasis = @PricingBasis
	AND PriceLoc = @PriceLoc
	AND ProductID = @ProductID
EXEC CheckProductPrice;3 @CurrBasePrice, @CurrBasePriceMT, @NewPrice
