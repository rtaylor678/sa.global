﻿CREATE PROCEDURE UpdateTransportation;2 (@StudyYear smallint, @Period char(2), 
	@CrudeOrigin smallint, @CrudeDest smallint, 
	@Action char(1), @AdjType char(2), @AdjValue real, @AdjValueMT real = NULL)
AS
IF @Action = 'I'
	INSERT INTO Transportation (StudyYear, Period, CrudeOrigin, CrudeDest, AdjType, AdjValue, AdjValueMT)
	VALUES (@StudyYear, @Period, @CrudeOrigin, @CrudeDest, @AdjType, @AdjValue, @AdjValueMT)
ELSE IF @Action = 'U'
	UPDATE Transportation 
	SET AdjValue = @AdjValue, AdjValueMT = @AdjValueMT
	WHERE StudyYear = @StudyYear 
	AND Period = @Period AND CrudeOrigin = @CrudeOrigin
	AND CrudeDest = @CrudeDest AND AdjType = @AdjType
ELSE IF @Action = 'D'
	DELETE FROM Transportation 
	WHERE StudyYear = @StudyYear 
	AND Period = @Period AND CrudeOrigin = @CrudeOrigin
	AND CrudeDest = @CrudeDest AND AdjType = @AdjType
