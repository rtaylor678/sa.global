﻿
CREATE PROCEDURE CheckCrudePrice;2 (
	@StudyYear smallint, @Period varchar(2), @CNum varchar(5), 
	@BasePrice real OUTPUT, @APIScale smallint OUTPUT, 
	@PostedAPI real OUTPUT, @CrudeOrigin smallint OUTPUT,
	@NewPrice tinyint OUTPUT, @PostedSulfur real OUTPUT, @SulfurAdjSlope real OUTPUT)
AS
DECLARE @CurrBasePrice real, @CurrAPIScale smallint, @CurrPostedAPI real, @CurrOrigin smallint
DECLARE @CurrPostedSulfur real, @CurrSulfurAdjSlope real
SET NOCOUNT ON
SELECT 	@CurrBasePrice = BasePrice, @CurrAPIScale = APIScale, 
	@CurrPostedAPI = PostedAPI, @CurrOrigin = CrudeOrigin,
	@CurrPostedSulfur = PostedSulfur, @CurrSulfurAdjSlope = SulfurAdjSlope
FROM CrudePrice
WHERE StudyYear = @StudyYear AND Period = @Period AND CNum = @CNum
IF @@ROWCOUNT = 0
BEGIN
	SELECT @NewPrice = 1, @PostedAPI = NULL, @APIScale = NULL, @BasePrice = NULL, @CrudeOrigin = NULL, 
	@PostedSulfur = NULL, @SulfurAdjSlope = NULL
END
ELSE
BEGIN
	SELECT @NewPrice = 0
	IF ABS(@PostedAPI - @CurrPostedAPI) < 0.01
		SELECT @CurrPostedAPI = NULL
	IF @APIScale = @CurrAPIScale
		SELECT @CurrAPIScale = NULL
	IF ABS(@BasePrice - @CurrBasePrice) < 0.01
		SELECT @CurrBasePrice = NULL
	IF @CrudeOrigin = @CurrOrigin
		SELECT @CurrOrigin = NULL
	IF NOT (@PostedSulfur IS NULL AND @CurrPostedSulfur IS NULL)
	BEGIN
		IF (@PostedSulfur IS NOT NULL) AND ABS(@PostedSulfur - @CurrPostedSulfur) < 0.001
			SELECT @CurrPostedSulfur = NULL
	END
	IF NOT (@SulfurAdjSlope IS NULL AND @CurrSulfurAdjSlope IS NULL)
	BEGIN
		IF (@CurrSulfurAdjSlope IS NULL) AND (@SulfurAdjSlope IS NOT NULL)
			SELECT @CurrSulfurAdjSlope = 0
		IF (@SulfurAdjSlope IS NOT NULL) AND ABS(@SulfurAdjSlope - @CurrSulfurAdjSlope) < 0.01
			SELECT @CurrSulfurAdjSlope = NULL
	END
	SELECT @NewPrice = 0, @PostedAPI = @CurrPostedAPI, 
		@APIScale = @CurrAPIScale, @BasePrice = @CurrBasePrice,
		@CrudeOrigin = @CurrOrigin, @PostedSulfur = @CurrPostedSulfur, @SulfurAdjSlope = @CurrSulfurAdjSlope
END

