﻿CREATE VIEW [dbo].[ShipStudyRates]
AS
SELECT r.PostingYear, ssr.Study, ssr.CargoSizeDWT, r.PostingPeriod, WorldScaleMult = r.WorldscaleRate*ssr.Multiplier/100
FROM ShipStudyRoutes ssr INNER JOIN ShipRateAvg r ON r.RouteNo = ssr.RouteNo


