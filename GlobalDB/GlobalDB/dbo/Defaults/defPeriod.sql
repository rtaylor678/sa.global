﻿CREATE DEFAULT [dbo].[defPeriod]
    AS 'TY';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defPeriod]', @objname = N'[dbo].[CrudePriceAlt].[Period]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defPeriod]', @objname = N'[dbo].[CrudePrice].[Period]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defPeriod]', @objname = N'[dbo].[Period]';

