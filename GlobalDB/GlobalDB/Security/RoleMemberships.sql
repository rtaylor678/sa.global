﻿ALTER ROLE [db_owner] ADD MEMBER [DC1\SupportDatabases_DBOwners];


GO
ALTER ROLE [db_owner] ADD MEMBER [ejb];


GO
ALTER ROLE [db_owner] ADD MEMBER [rvbDev];


GO
ALTER ROLE [db_owner] ADD MEMBER [rdt];


GO
ALTER ROLE [db_datareader] ADD MEMBER [PWRAPPS];


GO
ALTER ROLE [db_datareader] ADD MEMBER [rvbDev];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [rvbDev];

