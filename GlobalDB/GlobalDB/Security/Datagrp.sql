﻿CREATE ROLE [Datagrp]
    AUTHORIZATION [dbo];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\SupportDatabases_DataProcessors];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Pipeline_DBOwners];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\APCStudy_DataProcessors];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\APCStudy_DBOwners];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\APCStudy_Developers];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Chemicals_DataProcessors];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Chemicals_DBOwners];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Chemicals_Developers];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\NGPP_DataProcessors];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\NGPP_DBOwners];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\NGPP_Developers];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Refining_DataProcessors];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Refining_DBOwners];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Refining_Developers];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Pipeline_DataProcessors];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Pipeline_Developers];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Power_DataProcessors];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Power_DBOwners];


GO
ALTER ROLE [Datagrp] ADD MEMBER [DC1\Power_Developers];

